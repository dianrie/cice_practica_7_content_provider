package com.example.dianrie.cice_practica_7_content_provider

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.text.InputType
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.github.javiersantos.materialstyleddialogs.enums.Style


import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {
    var miDBHandler: MiDBHandler? = null
    var recyclerAdapter: RecyclerAdapterAndroidStorage? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        title = "BASE DE DATOS PRODUCTOS"
        miDBHandler = MiDBHandler(this, contentResolver)

        /**
         * RecyclerView -----------------------------------------
         *
         * Inicializamos le pasamos al RecyclerAdapter el miDBHanler y un array con los productos a mostrar
         * si no hay productos en la base de datos no se muestra el RecyclerList
         */
        var layoutManager: RecyclerView.LayoutManager? = null
        var adapter: RecyclerView.Adapter<RecyclerAdapterAndroidStorage.ViewHolder>? = null

       //layoutManager = GridLayoutManager(this, 2)
        layoutManager = LinearLayoutManager(this)
        //layoutManager = StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.HORIZONTAL)
        //Le paso al Recycler un array con los producto en la base de datos


        val array = miDBHandler?.findProduct(null)
        //Comprobamos si hay resultados
        if (array != null) {
            adapter = RecyclerAdapterAndroidStorage(miDBHandler!!, array)

            //Asignamos al RecyclerView el adapter
            miRecyclerView.layoutManager = layoutManager
            miRecyclerView.adapter = adapter

            recyclerAdapter = miRecyclerView.adapter as RecyclerAdapterAndroidStorage

            //------------------------------------------------------
        }
        // BOTON AÑADIR NUEVO PRODUCTO llama a la funcion addproduct para mostrar un alert con un formulario

        fab.setOnClickListener {
            addproduct(false, false, null)
        }
    }

    /**
     * addproduct -----------------------------------------
     *
     * Mustra un alert con dos EditTex nombre y cantidad para poder crear un nuevo producto
     * ademas Comprobamos si hay algun EditTex vacio y de ser asi se vuelve a cargar el
     * alert con un mensaje de error en el campo correspondiente
     *
     */
    fun addproduct(alertNombreNull: Boolean, alertCantidadNull: Boolean, valueSave: String?) {
        //Se crea un campo de texto para poder introducir un nombre
        val layout = LinearLayout(this)
        layout.orientation = LinearLayout.VERTICAL

        val nombre = EditText(this)
        nombre.hint = "Nombre"
        layout.addView(nombre)

        val cantidad = EditText(this)
        cantidad.hint = "Cantidad"
        cantidad.inputType = InputType.TYPE_CLASS_NUMBER
        layout.addView(cantidad)

        if (alertNombreNull) {
            nombre.error = "Please enter a valid Name."
            cantidad.setText(valueSave)
        }
        if (alertCantidadNull) {
            cantidad.error = "Please enter a valid quantity."
            nombre.setText(valueSave)
        }


        val alerta = MaterialStyledDialog.Builder(this)
        alerta.setTitle("Añadir producto:\n")
        alerta.setIcon(android.R.drawable.ic_input_add)
        alerta.setHeaderDrawable(R.drawable.basededatos2)
        alerta.setStyle(Style.HEADER_WITH_ICON)
        alerta.setCustomView(layout)
        alerta.setNegativeText("Cancelar")
        alerta.onNegative { dialog, which ->
            Toast.makeText(this, "No se añadio", Toast.LENGTH_LONG).show()
        }
        alerta.setPositiveText("Añadir")
        alerta.onPositive { dialog, which ->
            //Comprobamos si hay algun EditTex vacio y de ser asi se vuelve a cargar el
            // alert con un mensaje de error en el campo correspondiente
            if (TextUtils.isEmpty(nombre.text) || TextUtils.isEmpty(cantidad.text)) {
                if (TextUtils.isEmpty(nombre.text) && TextUtils.isEmpty(cantidad.text)) {
                    addproduct(true, true, null)
                } else {
                    if (TextUtils.isEmpty(nombre.text)) {
                        addproduct(true, false, cantidad.text.toString())
                    }
                    if (TextUtils.isEmpty(cantidad.text)) {
                        addproduct(false, true, nombre.text.toString())
                    }
                }
            } else {
                var product: Product? = null
                val nombre = nombre.text.toString()
                val cantidad = cantidad.text.toString().toInt()

                product = Product(nombre, cantidad)

                miDBHandler?.addProduct(product)

                if (recyclerAdapter == null) {
                    //si es el primer registro de la base de datos se llama a mainActivity para
                    // que muestre la recycler List
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                } else {
                    //Si no es el primer resitro de la base de datos se refresca la recycler list para
                    // que se muestre el producto añadido
                    recyclerAdapter?.refrescarAdapter(null, null)
                }
            }
        }
        alerta.show()
    }

    //Boton que busca el producto que contiene el EditTex y lo muestra en la RecyclerList
    fun findProduct(view: View) {
        val productosEncontrados = miDBHandler?.findProduct(productName.text.toString())
        //Comprobamos si hay resultados
        if (productosEncontrados != null) {
            recyclerAdapter?.refrescarAdapter(productosEncontrados, null)
        } else {
            recyclerAdapter?.refrescarAdapter(null, null)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.exit -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
