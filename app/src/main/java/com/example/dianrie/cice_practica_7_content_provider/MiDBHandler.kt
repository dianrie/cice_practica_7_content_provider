package com.example.dianrie.cice_practica_7_content_provider

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.widget.Toast


class MiDBHandler(var context: Context, var contentResolver: ContentResolver) {


    companion object {
        val AUTHORITY = "com.cice.formador.androidstorage.MyContentProvider"
        val PRODUCTS_TABLE = "products"

        val CONTENT_URI = Uri.parse("content://$AUTHORITY/$PRODUCTS_TABLE")

        val COLUMN_ID = "_id"
        val COLUMN_PRODUCTNAME = "productname"
        val COLUMN_QUANTITY = "quantity"
    }

    fun addProduct(product: Product) {
        val registro = ContentValues()
        registro.put(COLUMN_PRODUCTNAME, product.productName.toString())
        registro.put(COLUMN_QUANTITY, product.quantity.toString().toInt())
        contentResolver.insert(CONTENT_URI, registro)
        Toast.makeText(context, "Registro Añadido: ${product.productName}", Toast.LENGTH_LONG).show()
    }

    fun findProduct(productName: String?): Array<Product>? {
        var productList: MutableList<Product>?
        val projection = arrayOf(COLUMN_ID, COLUMN_PRODUCTNAME, COLUMN_QUANTITY)
        val selection: String?
        if (productName == null) {
            selection = null
        } else {
            selection = "productname = \"${productName}\""
        }
        val cursor = contentResolver.query(
            CONTENT_URI,
            projection,
            selection,
            null, null
        )

        //Comprobamos si hay resultados
        if (cursor.moveToFirst()) {
            cursor.moveToFirst()
            productList = mutableListOf<Product>()

            for (i in 1..cursor.count) {
                val id = Integer.parseInt(cursor.getString(0))
                val nombre = cursor.getString(1)
                val cantidad = Integer.parseInt(cursor.getString(2))

                productList.add(Product(id, nombre, cantidad))
                cursor.moveToNext()
            }
            cursor.close()
        } else {
            productList = null
            Toast.makeText(context, "Producto no encontrado", Toast.LENGTH_LONG).show()
        }
        return productList?.toTypedArray()
    }

    fun deleteProduct(productName: String): Int {
        var count: Int
        var selection = "productname = \"${productName}\""
        var rowsDeleted = contentResolver.delete(
            CONTENT_URI,
            selection, null
        )

        if (rowsDeleted > 0) {
            Toast.makeText(context, "Producto Eliminado: ${productName}", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, "Producto no encontrado", Toast.LENGTH_LONG).show()
        }
        return rowsDeleted
    }

    fun updateProduct(product: Product): Int {

        var devolver: Int
        val registro = ContentValues()
        registro.put(COLUMN_PRODUCTNAME, product.productName.toString())
        registro.put(COLUMN_QUANTITY, product.quantity.toString().toInt())
        devolver =
                contentResolver.update(CONTENT_URI, registro, "$COLUMN_ID = ${product.id.toString().toInt()}", null)

        Toast.makeText(context, "Producto Actualizado: ${product.productName} = ${product.quantity}", Toast.LENGTH_LONG)
            .show()
        return devolver
    }

}