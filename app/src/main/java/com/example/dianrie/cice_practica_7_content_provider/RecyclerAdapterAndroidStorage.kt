package com.example.dianrie.cice_practica_7_content_provider


import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.github.javiersantos.materialstyleddialogs.enums.Style


class RecyclerAdapterAndroidStorage(var miDBHandler: MiDBHandler, var arrayProducts: Array<Product>) :
    RecyclerView.Adapter<RecyclerAdapterAndroidStorage.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerAdapterAndroidStorage.ViewHolder {
        val vistaCelda = LayoutInflater.from(p0.context).inflate(R.layout.card_layout_main_activity, p0, false)
        return ViewHolder(vistaCelda)
    }

    override fun getItemCount(): Int {
        return arrayProducts.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.idProduct.text = arrayProducts[p1].id.toString()
        p0.nombreProduct.text = arrayProducts[p1].productName
        p0.cantidadProduct.text = arrayProducts[p1].quantity.toString()

    }

    //Funcion para actualizar la lista despues de hacer cualquier modificacion en la base de datos
    fun refrescarAdapter(arrayProductos: Array<Product>?, context: Context?) {
        var array: Array<Product>?

        if (arrayProductos != null) {
            array = arrayProductos
        } else {
            array = miDBHandler?.findProduct(null)
        }
        //Comprobamos si hay resultados
        if (array != null) {
            arrayProducts = array
        } else {
            //si no hay registros en la base de datos se recarga el main Activity sin el recycler
            val intent = Intent(context, MainActivity::class.java)
            context?.startActivity(intent)

        }
        notifyDataSetChanged()

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var idProduct: TextView
        var nombreProduct: TextView
        var cantidadProduct: TextView
        var borrarButton: ImageView
        var actualizarButton: ImageView

        init {
            idProduct = itemView.findViewById(R.id.textView9)
            nombreProduct = itemView.findViewById(R.id.textView6)
            cantidadProduct = itemView.findViewById(R.id.textView8)
            borrarButton = itemView.findViewById(R.id.imageView)
            actualizarButton = itemView.findViewById(R.id.imageView2)

            //Celda Listener para pasar datos a FormulariActuvity si se seleciona la celda
            /*itemView.setOnClickListener {
                Toast.makeText(itemView.context, "Elige", Toast.LENGTH_SHORT).show()

            }*/
            // Listener para el boton borrar de cada celda
            borrarButton.setOnClickListener {
                miDBHandler?.deleteProduct(nombreProduct.text.toString())
                //Toast.makeText(itemView.context, "BORRADO", Toast.LENGTH_SHORT).show()
                refrescarAdapter(null, itemView.context)
            }

            // Listener para el boton actualizar de cada celda
            actualizarButton.setOnClickListener {

                //Se crea un campo de texto para poder introducir un nombre
                val layout = LinearLayout(itemView.context)
                layout.orientation = LinearLayout.VERTICAL


                val nombre = EditText(itemView.context)
                nombre.setText(nombreProduct.text.toString())
                layout.addView(nombre)
                val cantidad = EditText(itemView.context)
                cantidad.inputType = InputType.TYPE_CLASS_NUMBER
                cantidad.setText(cantidadProduct.text.toString())
                layout.addView(cantidad)


                val alerta = MaterialStyledDialog.Builder(itemView.context)
                alerta.setTitle("Editar producto:\n")
                alerta.setIcon(android.R.drawable.ic_menu_edit)
                alerta.setHeaderDrawable(R.drawable.basededatos2)
                alerta.setStyle(Style.HEADER_WITH_ICON)
                alerta.setCustomView(layout)

                alerta.setNegativeText("Cancelar")
                alerta.onNegative { dialog, which ->
                    Toast.makeText(itemView.context, "No se Modifico", Toast.LENGTH_LONG).show()
                }
                alerta.setPositiveText("Modificar")
                alerta.onPositive { dialog, which ->

                    //Comprobamos si hay algun text edit vacio y de ser asi se vuelve a cargar el
                    // alert con un mensaje de error en el campo correspondiente
                    if (TextUtils.isEmpty(nombre.text) || TextUtils.isEmpty(cantidad.text)) {
                        if (TextUtils.isEmpty(nombre.text) && TextUtils.isEmpty(cantidad.text)) {
                            //addproduct(true, true, null)
                            Toast.makeText(
                                itemView.context,
                                "Registro no modificado :\nNombre y cantidad no pueden estar vacios",
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            if (TextUtils.isEmpty(nombre.text)) {
                                //addproduct(true, false, cantidad.text.toString())
                                Toast.makeText(
                                    itemView.context,
                                    "Registro no modificado :\nNombre  no puede estar vacio",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                            if (TextUtils.isEmpty(cantidad.text)) {
                                //addproduct(false, true, nombre.text.toString())
                                Toast.makeText(
                                    itemView.context,
                                    "Registro no modificado :\nCantidad no puede estar vacio",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    } else {
                        val id = idProduct.text.toString().toInt()
                        val nombre = nombre.text.toString()
                        val cantidad = cantidad.text.toString().toInt()
                        val product = Product(id, nombre, cantidad)

                        miDBHandler?.updateProduct(product)
                        var array: Array<Product> = arrayOf(Product(id, nombre, cantidad))
                        refrescarAdapter(array, null)
                    }
                }
                alerta.show()


            }
        }
    }
}